### Kata String Calculator

The kata can be found at: 
http://katayuno-app.herokuapp.com/katas/5

- Language: JavaScript
- Testing Tool: Jest

To launch the test type : ```npm test``` in your console.