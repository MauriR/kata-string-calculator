describe('String Calculator', () => {
  it('Does something', () => {
    expect(true).toBe(true)
  })
})

const FIRST_NUMBER = 2
const SECOND_NUMBER = 3
const EMPTY_STRING = ""
const FOURTH_NUMBER = 1
const FIRST_PLUS_SECOND = FIRST_NUMBER + SECOND_NUMBER
const TOTAL_SUM = 6


describe('Step 1: Create a simple String calculator', () => {
  it('returns 0 if an empty string is given', () => {

    const sum = stringCalculator("")

    expect(sum).toBe(0)
  })
  it('returns the given number', () => {

    const sum = stringCalculator(FIRST_NUMBER)

    expect(sum).toBe(FIRST_NUMBER)
  })

  it('accepts two numbers and sums them', () => {

    const sum = stringCalculator(FIRST_NUMBER, SECOND_NUMBER)

    expect(sum).toBe(FIRST_PLUS_SECOND)
  })
})

describe('Step 2: handle an unknown amount of numbers', () => {
  it('Allows the String calculator to handle an unknown amount of numbers.', () => {

    const sum = stringCalculator(FIRST_NUMBER, SECOND_NUMBER, EMPTY_STRING, FOURTH_NUMBER)

    expect(sum).toBe(TOTAL_SUM)
  })
})


function stringCalculator(...addends) {
  let sumList = []
  for (addend of addends) {
    if (addend == EMPTY_STRING) {
      addend = 0
    }
    sumList.push(addend)
  }
  return sumList.reduce((previous, current) => {
    return previous + current;
  })
}

// Preguntas Vicente : Cómo puedo refactorizar el if. Si lo saco a una función
//distinta, no sabrá lo que es addend. 



// const reducer = (accumulator, currentValue) => accumulator + currentValue